import setuptools

#with open("README.md", "r", encoding="utf-8") as fh:
#    long_description = fh.read()

setuptools.setup(
    name='python_enigma_bar',
    version='0.0.3',
    author='Arwinder Singh',
    author_email='arwindersingh021@gmail.com',
    description='Virtual Enigma Machine',
    #"long_description=long_description,
    #long_description_content_type="text/markdown",
    url='https://gitlab.com/arwindersngh62/python-enigma.git',
    #project_urls = {
    #    "Bug Tracker": "https://github.com/mike-huls/toolbox/issues"
    #},
    license='GNU GPLv3',
    packages=['python_enigma_bar'],
    install_requires=[],
)